import json

from fuzzywuzzy import fuzz
from openpyxl import load_workbook


def json_to_py(file_name):
    with open(file_name) as file:
        data = json.load(file)
    
    return data

onep_skus = json_to_py("1p_skus.json")
model_item_master = json_to_py("model_item_master.json")

onep_skus_manufacturer_names = [item.get("manufacturer") for item in onep_skus if item.get("manufacturer")]
onep_skus_manufacturer_names = list(set(onep_skus_manufacturer_names))
model_item_master_manufacturer_names = [item.get("manufacturer") for item in model_item_master if item.get("manufacturer")]
model_item_master_manufacturer_names = list(set(model_item_master_manufacturer_names))


def normalization(in_list):
    out_list = []

    for i in in_list:
        replaced_str = (" ".join(i.split())).lower()
        replacable_strings = {"pvt":"private", "pvt.":"private", "ltd":"limited", "(p)":"private", "&":"and", "co.":"c_name", ",":"", ".":"", "-":""}
        for key, value in replacable_strings.items():
            replaced_str = replaced_str.replace(key, value)
            
        if (("(" and ")") in replaced_str):
            replaced_str = replaced_str.replace(replaced_str[ replaced_str.find("(") : replaced_str.find(")") + 1], "")

        replaced_str = (" ".join(replaced_str.split()))
        out_list.append(replaced_str)

    return out_list


normalized_onep_skus_manufacturer_names = normalization(onep_skus_manufacturer_names)
normalized_model_item_master_manufacturer_names = normalization(model_item_master_manufacturer_names)

normalized_onep_skus_manufacturer_names = list(set(normalized_onep_skus_manufacturer_names))
normalized_model_item_master_manufacturer_names = list(set(normalized_model_item_master_manufacturer_names))

matched_onep_skus_manufacturer_names = []

def match_manufacturer_names(matched_onep_skus_manufacturer_names, list1, list2, percentage_start, percentage_end):
    matched_model_manufacturer_names = []

    # Appending empty rows to make excel writes consistent
    for i in range(len(matched_onep_skus_manufacturer_names)):
        matched_model_manufacturer_names.append("")

    for item1 in list1:
        for item2 in list2:
            if percentage_start == percentage_end:
                if fuzz.token_sort_ratio(item1, item2) == percentage_start:
                    matched_onep_skus_manufacturer_names.append(item1)
                    matched_model_manufacturer_names.append(item2)
            else:
                if ((fuzz.token_sort_ratio(item1, item2) >= percentage_start) and (fuzz.token_sort_ratio(item1, item2) < percentage_end)):
                    matched_onep_skus_manufacturer_names.append(item1)
                    matched_model_manufacturer_names.append(item2)

    return matched_model_manufacturer_names


matched_manufacturer_names_100 = match_manufacturer_names(matched_onep_skus_manufacturer_names, normalized_onep_skus_manufacturer_names, normalized_model_item_master_manufacturer_names, 100, 100)
matched_manufacturer_names_95 = match_manufacturer_names(matched_onep_skus_manufacturer_names, normalized_onep_skus_manufacturer_names, normalized_model_item_master_manufacturer_names, 95, 100)
matched_manufacturer_names_90 = match_manufacturer_names(matched_onep_skus_manufacturer_names, normalized_onep_skus_manufacturer_names, normalized_model_item_master_manufacturer_names, 90, 95)
matched_manufacturer_names_85 = match_manufacturer_names(matched_onep_skus_manufacturer_names, normalized_onep_skus_manufacturer_names, normalized_model_item_master_manufacturer_names, 85, 90)

# Appending empty rows to make excel writes consistent
def appending_empty_rows(list1, list2):
    while(len(list1) != len(list2)):
        list1.append("")
    
    return list1

matched_manufacturer_names_100 = appending_empty_rows(matched_manufacturer_names_100, matched_onep_skus_manufacturer_names)
matched_manufacturer_names_95 = appending_empty_rows(matched_manufacturer_names_95, matched_onep_skus_manufacturer_names)
matched_manufacturer_names_90 = appending_empty_rows(matched_manufacturer_names_90, matched_onep_skus_manufacturer_names)
matched_manufacturer_names_85 = appending_empty_rows(matched_manufacturer_names_85, matched_onep_skus_manufacturer_names)

# Write to workbook
def write_to_excel(workbook_name, worksheet_name):
    workbook = load_workbook(workbook_name)
    worksheet = workbook[worksheet_name]
    worksheet.append(["1p_skus", "model_item_master (100)", "model_item_master (95)", "model_item_master (90)", "model_item_master (85)"])

    for i in range(len(matched_onep_skus_manufacturer_names)):
        worksheet.append([matched_onep_skus_manufacturer_names[i], matched_manufacturer_names_100[i], matched_manufacturer_names_95[i], matched_manufacturer_names_90[i], matched_manufacturer_names_85[i]])

    workbook.save(workbook_name)

    return True

result = write_to_excel("normalized_data.xlsx", "normalized_data")
